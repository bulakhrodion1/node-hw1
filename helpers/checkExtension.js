const checkExtension = (str) => {
    return str.match(/(?<=\.)[^.]+$/)[0].toLowerCase() || "";
};

module.exports = checkExtension;
