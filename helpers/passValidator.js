const bcrypt = require("bcryptjs");

const checkPassword = async (passwords, password, filename) => {
    return !(
        passwords[filename] &&
        (password === undefined ||
            !(await bcrypt.compare(String(password), passwords[filename])))
    );
};

const isValidPassword = async (passwords, password, filename, res) => {
    try {
        const validPassword = await checkPassword(passwords, password, filename);
        if (!validPassword) {
             res.status(400).json({
                message: `Wrong password for file ${filename}`,
            });
        }
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: "Internal server error",
        });
    }
};

module.exports = isValidPassword;