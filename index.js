const fs = require("fs");
const path = require("path");
const express = require("express");
const cors = require("cors");

const router = require("./routes/mainRoutes");

const dir = path.join(__dirname, "api", "files");
const apiFolder = path.join(__dirname, "api");

try {
    if (!fs.existsSync(apiFolder)) {
        fs.mkdirSync(apiFolder);
    }
} catch (error) {
    console.log(error);
}


try {
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }
} catch (error) {
    console.log(error);
}

const PORT = 8080;
const app = express();

app.use(express.json());
app.use(
    cors({
        origin: "*",
        methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
        allowedHeaders: "Content-Type",
    })
);

app.use("/api/files", router);

app.listen(PORT, (err) => {
    if (err) {
        console.log(err);
    }
    console.log("server is running at port " + PORT);
});
